function Get-DayofWeek {
    param (

    )
    
    # Get the name of each day of the week
    $dayOfWeek = for($counter = 0; $counter -le 6; $counter ++){
        (get-date).AddDays($counter).DayOfWeek
    } 
    
    # Output days so user can select relevant day
    $dayOfWeek | Out-GridView -PassThru

}