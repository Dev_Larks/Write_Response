function Get-EmailDomain {
    [CmdletBinding()]
    param (
        
    )
    
    begin {
        
    }
    
    process {
        $selection = Get-Content C:\TMP\ScriptInput\EmailDomains.txt | Out-GridView -Title 'Select the new email domain' -PassThru

        $selection
    }
    
    end {
        
    }
}