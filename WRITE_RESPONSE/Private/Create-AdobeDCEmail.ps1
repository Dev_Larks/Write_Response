function Write-AdobeDCEmail {
    param (
        [string] $recipient
    )
    
    # Solution found from : https://stackoverflow.com/questions/24044681/powershell-open-email-draft-with-signature

    $Outlook = New-Object -comObject Outlook.Application
    $TlabEmail = $Outlook.CreateItem(0)
    $TlabEmail.GetInspector.Activate()
    $signature = $TlabEmail.HTMLBody
    $TlabEmail.To = $recipient
    $TlabEmail.Subject = "Signing into Adobe Acrobat DC using an Enterprise ID"
    $body = "<font face ='calibri' size='2'> Good Morning Kevin <br><br> Please find attached instructions for signing into Adobe Acrobat DC using an Enterprise ID. If you encounter any problems completing this step please let me know. <br><br> The application should be visible in your start menu within 2 hours of your first login on the PAG device."
    $TlabEmail.HTMLBody = $body + $signature
    $file = "C:\Users\LARKINC\OneDrive - NSWGOV\Service Delivery\Help Documentation\HowTo - DCS Adobe Acrobat - Signing in with Federated ID.pdf"
    $TlabEmail.Attachments.Add($file)
    $TlabEmail.save()

}