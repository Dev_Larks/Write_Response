function Get-SelectedSoftware {
        param (
        [string] $application
    )
    
    begin {
        
    }
    
    process {
        if ($applications.Length -eq '3') {

            # Call private helper function to get day of the week
            $day = Get-DayofWeek
        
            # Seperate $application string into app values
            $adobe = $applications[0].Trim()
            $project = $applications[1].Trim()
            $visio = $applications[2].Trim()
            $nameOne = $adobe.Substring(0,13)+$adobe.Substring(26,3)
        
            $output = "`nI will close off this request as the ticket I logged on your behalf for you to be assigned a user license for $adobe, $project and $visio has now been completed. `n`nYou have been assigned a user license for $project and $visio both applications should be visible in your Windows start menu and are named 'Project' and 'Visio'.`n`nOn $day I sent instructions on the process to sign into the $adobe application using an Enterprise ID. This application should also be visible in your Windows start menu and is named $nameOne.`n`nIf you encounter any issues signing into the products please reply back to this email to reopen this ticket."
        
        }
        elseif ($applications.Length -eq '2') {
        
            # Change original string and reconcatenate so actual length can be evaluated.
            $application = $applications[0].Trim() + ' ' + $applications[1].Trim()
            
            if ($application.Length -eq 52) {
                $appString = "Adobe Acrobat Professional DC and Microsoft Project"
                $appOne = $appString.Substring(0,29)
                $appTwo = $appString.Substring(34,17)
                $nameOne = $appOne.Substring(0,13)+$appOne.Substring(26,3)
                $nameTwo = $appTwo.Substring(10,7)
        
            }
            elseif ($application.Length -eq 50) {
                $appString = "Adobe Acrobat Professional DC and Microsoft Visio"
                $appOne = $appString.Substring(0,29)
                $appTwo = $appString.Substring(34,15)
                $nameOne = $appOne.Substring(0,13)+$appOne.Substring(26,3)
                $nameTwo = $appTwo.Substring(10,5)
            }
            else {
                $appString= "Microsoft Project and Visio"
                $appOne = $appString.Substring(0,17)
                $appTwo = $appString.Substring(22,5)
                $nameOne = $appOne.Substring(10,7)
                $nameTwo = $appTwo
            }
        
            $output = "`nI will close off this request as the ticket I logged on your behalf for you to be assigned a user license for $appString has now been completed.`n`nYou have been assigned a user license for $appOne and $appTwo both applications should be visible in your Windows start menu and are named ""$nameOne"" and ""$nameTwo""."
            if ($appOne.Length -eq '29') {
        
                # Call private helper function to get day of the week
                $day = Get-DayofWeek
        
                $output += "`n`nOn $day I sent instructions on the process to sign into the $appOne application using an Enterprise ID.`n`nIf you encounter any issues signing into the products please reply back to this email to reopen this ticket."
            }
            else {
                $output += "`n`nIf you encounter any issues signing into the products please reply back to this email to reopen this ticket."
            }
        }
        elseif ($application -match 'Adobe Acrobat Professional DC') {
            
            # Call private helper function to get day of the week
            $day = Get-DayofWeek
        
            $output = "`nI will close off this request as the ticket I logged on your behalf for you to be assigned a user license for $application has now been completed. `n`nI sent you instructions on $day with the process to sign into the $application application using an Enterprise ID.`n`nThe application should be visible in your Windows start menu and is named 'Adobe Acrobat DC'`n`nIf you encounter any issues signing into the product please reply back to this email to reopen this ticket."
        
        }
        elseif ($application -match "Microsoft Project 2016" -or $application -eq "Microsoft Visio 2016") {
        
            # Get the second word in the application name string
            # https://community.idera.com/database-tools/powershell/ask_the_experts/f/learn_powershell_from_don_jones-24/13721/how-to-find-nth-word-in-a-line-or-string
            $index = 2
            $appName = ($application -split "\s+",($index +1 ))[($index-1)]
        
            $output = "`nI will close off this request as the ticket I logged on your behalf for you to be assigned a user license for $application has now been completed. `n`nThe application should be visible in your Windows start menu and is named '$appName'`n`nIf you encounter any issues signing into the product please reply back to this email to reopen this ticket."
        }

        $output
    }
    
    end {
        
    }
}





