function Get-UserGroups {

    [CmdletBinding()]
    param (
        [string] $userId
    )

    $groups = Get-ADPrincipalGroupMembership -Identity $compare -Server $server | 
        Where-Object { $_.Name -match '^Intune' } | Select-Object name | Sort-Object name

    $groups
}