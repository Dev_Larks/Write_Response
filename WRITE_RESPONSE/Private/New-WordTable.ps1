function New-WordTable {
    [CmdletBinding()]
    param (
        
    )
    
    begin {
        
        # Check if JobNotes.docx is already open and close if necessary
        # https://stackoverflow.com/questions/18191431/how-to-quit-or-close-not-kill-word-document-process
        $wd = [Runtime.Interopservices.Marshal]::GetActiveObject('Word.Application')
        $wd.Documents | Where-Object { $_.Name -eq 'JobNotes.docx' } | ForEach-Object {
            #$_.Saved = $true
            $_.Close()
        }
    }
    
    process {

        $status = 'In Progress'

        $date = get-date -Format "dd/MM/yyyy"

        $requestID = Read-Host 'Please provide the request id'

        $service = Get-Content C:\TMP\ScriptInput\ServiceCatalog.txt | Out-GridView -PassThru

        $user = "$userName `n[$userid]"

        [ref]$SaveFormat = "microsoft.office.interop.word.WdSaveFormat" -as [type]

        $topics = Import-Csv C:\TMP\ScriptInput\JobClosure.txt

        $path = "C:\TMP\ScriptOutput\JobNotes.docx"

        $Number_Of_Rows = 2

        $Number_Of_Columns = ($topics | gm -MemberType NoteProperty).count

        $x = 2

        $Word = New-Object -comobject word.application

        $Word.Visible = $false

        $Doc = $Word.Documents.Add()

        $Range = $Doc.Range()

        $TableBehavior = [Microsoft.Office.Interop.Word.WdDefaultTableBehavior]::wdWord9TableBehavior

        $Doc.Tables.Add($Range, $Number_Of_Rows, $Number_Of_Columns, $TableBehavior) | Out-Null

        $Table = $Doc.Tables.item(1)

        $Table.Cell(1, 1).Range.Text = "User"

        $Table.Cell(1, 2).Range.Text = "Service"

        $Table.Cell(1, 3).Range.Text = "Request ID"

        $Table.Cell(1, 4).Range.Text = "Date Logged"

        $Table.Cell(1, 5).Range.Text = "Status"

        $Table.Cell($x, 1).Range.Text = $user
        $Table.Cell($x, 2).Range.Text = $service
        $Table.Cell($x, 3).Range.Text = $requestID
        $Table.Cell($x, 4).Range.Text = $date
        $Table.Cell($x, 5).Range.Text = $status

        $doc.saveas([ref] $path, [ref]$SaveFormat::wdFormatDocumentDefault)

        $doc.close()

        $word.quit()
        
    }
    
    end {
        Invoke-Item C:\TMP\ScriptOutput\JobNotes.docx
    }
}