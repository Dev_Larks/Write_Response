function Get-Greeting {
    param (

    )
    
    # Check if morning or afternoon
    if ((Get-Date -UFormat %p) -eq 'AM') {
        $start = "Good Morning $firstName"
    } 
    else {
        $start = "Good Afternoon $firstName"
    }

    $start
}