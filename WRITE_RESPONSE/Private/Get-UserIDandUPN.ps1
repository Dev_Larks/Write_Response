function Get-UserIDandUPN {

    [CmdletBinding()]
    param (
        [string] $Username
    )
    
    BEGIN {

    }

    PROCESS {
        
        # Check all user accounts that match the name provided return only those that are in a state of enabled
        Write-Verbose "Checking for user accounts that match $username"
        $identity = Get-ADUser -Filter "(DisplayName -like '$username') -and (Enabled -eq 'True')" -Server $server

        $email = $identity.UserPrincipalName
        $email
        $name = $identity.UserPrincipalName -split '@'
        $name
        $identity = $identity.Name
        $identity

    }

    END {

    }

}