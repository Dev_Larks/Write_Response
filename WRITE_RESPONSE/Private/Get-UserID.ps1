function Get-UserID {

    [CmdletBinding()]
    param (
        [string] $Username
    )
    
    BEGIN {

        $domain = (((Get-ADDomain -Server $server).name).toUpper())

    }

    PROCESS {
        
        # Check all user accounts that match the name provided return only those that are in a state of enabled
        Write-Verbose "Checking for user accounts that match $username"
        $identity = Get-ADUser -Filter "(DisplayName -like '$username') -and (Enabled -eq 'True')" -Server $server

        # Check to see if user object found that matches search criteria
        if ($null -eq $identity) {
            Write-Warning "No user found in $domain AD with that name, checking for user accounts that match the surname"

            # Search for accounts using the surname of the user provided
            Write-Verbose "Checking for accounts matching the provided users surname"
            $surname = ($username.Split('')[1])
            $identity = Get-ADUser -Filter { Surname -like $surname } -Server $server -Properties DistinguishedName, GivenName, Surname, DisplayName, Name | Select-Object Name, GivenName, Surname, DisplayName, UserPrincipalName, DistinguishedName

            # Check to see if user object found that matches the surname
            if ($null -eq $identity) {
                Write-Warning "No user account for $Username found in $domain AD, check the username spelling"
                break
            }
        
            # If multiple matches on the surname found output to window to select user account
            elseif ($identity.count -gt 1) {
                Write-Verbose "Select the user name from the output window to continue"
                $identity = (($identity | Out-GridView -PassThru).Name)
                $identity
            } 
            else {
                $identity = $identity.Name
                $identity
            }
        }
        
        # If multiple matches on the users name then output to window to select user account
        elseif ($identity.count -gt 1) {
            Write-Verbose "Select the user name from the output window to continue"
            $identity = (($identity | Out-GridView -PassThru).Name)
            $identity
        }
        
        else {
            $identity = $identity.Name
            $identity
        }

    }

    END {

    }

}