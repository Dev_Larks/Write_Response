function Get-LicensedSoftware {
    [CmdletBinding()]
    param (
        
    )
    
    begin {
        
    }
    
    process {
        $selection = Get-Content C:\TMP\ScriptInput\Applications.txt | Out-GridView -Title 'Select one or more applications to license' -PassThru

        $selection
    }
    
    end {
        
    }
}