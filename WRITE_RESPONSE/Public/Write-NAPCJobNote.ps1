function Write-NAPCJobNote {
    [CmdletBinding()]
    param (
        [string] $status = 'In Progress',

        [Parameter(Mandatory = $true)]
        [string] $drive,

        [Parameter(Mandatory=$true)] 
        [string] $requestID,

        [Parameter(Mandatory = $true)] 
        [string] $userName = "John Forrest [FORRESTJ]"
    )
    
    begin {

        $date = get-date -Format "dd/mm/yyyy"

        $service = "$drive Drive Access"

        [ref]$SaveFormat = "microsoft.office.interop.word.WdSaveFormat" -as [type]

        $topics = Import-Csv C:\TMP\ScriptInput\JobClosure.txt
        
        $path = "C:\TMP\ScriptOutput\JobClosure.docx"
        
        $Number_Of_Rows = 2
        
        $Number_Of_Columns = ($topics | Get-Member -MemberType NoteProperty).count
        
        $Word = New-Object -comobject word.application
        
        $Word.Visible = $false
        
        $Doc = $Word.Documents.Add()
        
        $Range = $Doc.Range()
        
        $Doc.Tables.Add($Range, $Number_Of_Rows, $Number_Of_Columns, $TableBehavior) | Out-Null
    }
    
    process {

        # Get the first two words in the provided string
        $user = ($username -split ' ')[0, 1] -join ' '

        # Get the third word in the userName string
        # https://community.idera.com/database-tools/powershell/ask_the_experts/f/learn_powershell_from_don_jones-24/13721/how-to-find-nth-word-in-a-line-or-string

        $index = 3
        $id = ($userName -split "\s+",($index +1 ))[($index-1)]

        $Table = $Doc.Tables.item(1)

        $Table.Cell(1, 1).Range.Text = "User"

        $Table.Cell(1, 2).Range.Text = "Service"

        $Table.Cell(1, 3).Range.Text = "Request ID"

        $Table.Cell(1, 4).Range.Text = "Date Logged"

        $Table.Cell(1, 5).Range.Text = "Status"

        $Table.Cell(2, 1).Range.Text = "$user`n$id"
        $Table.Cell(2, 2).Range.Text = $service
        $Table.Cell(2, 3).Range.Text = $requestID
        $Table.Cell(2, 4).Range.Text = $date
        $Table.Cell(2, 5).Range.Text = $status

        $Table.AutoFormat(0)
        $Table.Borders.InsideLineStyle = 1
        $Table.Borders.OutsideLineStyle = 1

        $doc.saveas([ref] $path, [ref]$SaveFormat::wdFormatDocumentDefault)

        $doc.close()

        $word.quit()
    }
    
    end {
        Invoke-Item $path
    }
}