function Write-addLicensedSoftware {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]   
        [string] $userName,
        
        [Parameter(Mandatory)]
        [string] $approver 
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        
        # Write verbose output to console
        Write-Verbose "Querying $UserName's AD account UserID"

        # Call private helper function to get UserID
        $userId = Get-UserID -UserName $UserName

        # Call private helper function to get required licensed software
        $software = Get-LicensedSoftware

        $end = "Thanks, Craig"

        $output = "Please find attached signed approval from $approver for $username [$userId] to be assigned a licence for $software.`n"

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\LicensedSoftwareOnboardingOutput.txt
        Add-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareOnboardingOutput.txt -Value `n$end

        # Write verbose output to console
        Write-Verbose "Output now saved to clipboard"
    }
    
    end {
        Get-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareOnboardingOutput.txt | clip
    }
}
