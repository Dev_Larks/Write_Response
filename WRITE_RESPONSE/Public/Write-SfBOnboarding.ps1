function Write-SfBOnboarding {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [string] $UserName
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        
        # Write verbose output to console
        Write-Verbose "Querying $UserName's AD account UserID"

        # Call private helper function to get UserID
        $userId = Get-UserID -UserName $UserName

        $end = "Thanks, Craig"

        $output = "I would like to request $username [$userId] to be enabled with Skype for Business with Enterprise Voice and Unified Messaging.`n"

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\SfBOnboardingOutput.txt
        Add-Content -Path C:\TMP\ScriptOutput\SfBOnboardingOutput.txt -Value `n$end
    }
    
    end {
        Invoke-Item -Path C:\TMP\ScriptOutput\SfBOnboardingOutput.txt
        New-WordTable
    }
}