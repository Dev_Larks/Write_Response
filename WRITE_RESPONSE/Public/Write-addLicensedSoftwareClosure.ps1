function Write-addLicensedSoftwareClosure {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]   
        [string] $userName
    )
    
    begin {
        
    }
    
    process {

        # Call private helper function to get salutation
        $start = Get-Greeting

        $end = "Kind Regards `n`nCraig"

        # Call private helper function to get licensed software applications\
        Write-Verbose "Select application from the dialog window in the taskbar"
        $applications = Get-LicensedSoftware

        $output = Get-SelectedSoftware -application $applications

        # Write request data to file
        Set-Content -Value $start -Path C:\TMP\ScriptOutput\LicensedSoftwareClosure.txt -NoNewline
        Add-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareClosure.txt -Value $userName 
        Add-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareClosure.txt -Value $output
        Add-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareClosure.txt -Value `n$end

        # Write verbose output to console
        Write-Verbose "Output now saved to clipboard"
         
    }
    
    end {
        Get-Content -Path C:\TMP\ScriptOutput\LicensedSoftwareClosure.txt | clip
    }
}