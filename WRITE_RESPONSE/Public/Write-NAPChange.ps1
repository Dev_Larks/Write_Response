function Write-NAPChange {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]   
        [string] $userName,
        [string] $approver,
        [string] $userToCompare,
        [string] $drive
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {

        # Write verbose output to console
        Write-Verbose "Querying $UserName's AD account UserID"

        # Call private helper function to get UserID
        $userId = Get-UserID -UserName $UserName

        # Call private helper function to get UserID of user to duplicate drive access from
        $compare = Get-UserID -UserName $userToCompare

        $end = "Thanks, Craig"

        $output = "Please find attached signed approval from $approver for $username [$userId] to be provided the same $drive access as $userToCompare [$compare].`n"

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\NAPCOutput.txt
        Add-Content -Path C:\TMP\ScriptOutput\NAPCOutput.txt -Value `n$end
    }
    
    end {
        Invoke-Item -Path C:\TMP\ScriptOutput\NAPCOutput.txt
    }
}