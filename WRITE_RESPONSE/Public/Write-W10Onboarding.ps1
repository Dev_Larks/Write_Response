
function Write-W10Onboarding {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory)]
        [string] $UserName,

        [Parameter(Mandatory)]
        [string] $GetUserGroups
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        
        # Write verbose output to console
        Write-Verbose "Querying $UserName's AD account UserID"

        # Call private helper function to get UserID
        $userId = Get-UserID -UserName $UserName

        # Write verbose output to console
        Write-Verbose "Querying $GetUserGroups's AD account UserID"

        # Call private helper function to get UserID of user to duplicate W10 groups from
        $compare = Get-UserID -UserName $GetUserGroups

        # Write verbose output to console
        Write-Verbose "Retrieving $GetUserGroups's W10 AD groups"

        # Call private function to get Intune groups from user to duplicate
        $groups = Get-UserGroups -userId $compare   
        
        # Final line of script output
        $end = "Thanks, Craig"

        $output = "Could you please onboard $username [$userId] for Windows 10, please add them to the list of W10 Intune AD groups provided below:`n" 

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\W10OnboardingOutput.txt
        Add-Content -Path C:\TMP\ScriptOutput\W10OnboardingOutput.txt -Value $groups.name
        Add-Content -Path C:\TMP\ScriptOutput\W10OnboardingOutput.txt -Value `n$end

    }
    
    end {
        Invoke-Item -Path C:\TMP\ScriptOutput\W10OnboardingOutput.txt
    }
}
