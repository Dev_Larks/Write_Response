function Write-removeAADDevice {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]   
        [string[]] $deviceName,

        [switch] $plural

    )
    
    begin {

    }
    
    process {

        $devices = $deviceName -join "`n"

        $end = "Thanks, Craig"

        if ($plural) {
            $output = "Please remove the following devices from Endpoint Manager and AAD as they are going to be rebuilt: `n"
        } else {
            $output = "Please remove the following device from Endpoint Manager and AAD as it is going to be rebuilt: `n"
        }
        

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\AADDelete.txt
        Add-Content -Path C:\TMP\ScriptOutput\AADDelete.txt -Value `n$devices`n
        Add-Content -Path C:\TMP\ScriptOutput\AADDelete.txt -Value `n$end
    }
    
    end {
        Get-Content -Path C:\TMP\ScriptOutput\AADDelete.txt | clip
    }
}
