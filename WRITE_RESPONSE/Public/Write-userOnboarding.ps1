function Write-userOnboarding {
    [CmdletBinding()]
    param (
        
    )
    
    begin {
        
    }
    
    process {
        
        $output = "W10 Onboarding - `nW10 + O365 licensing - `nSfB - `nEmail address - "

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\userOnboarding.txt

    }
    
    end {

        Get-Content -Path C:\TMP\ScriptOutput\userOnboarding.txt | clip
        
    }
}