function write-emailAddressChange {
    [CmdletBinding()]
    param (
        [string] $userName
    )
    
    begin {
        if (!(Get-Module ActiveDirectory)) {
            Import-Module ActiveDirectory
        }
    }
    
    process {
        
        # Write verbose output to console
        Write-Verbose "Querying $userName's AD account UserID"

        # Call private helper function to get UserID
        $userId = Get-UserIDandUPN -UserName $userName

        # Call private helper function to get email domain
        $domain = Get-EmailDomain

        $end = "Thanks, Craig"

        $email = $userId[0]
        $displayName = $userId[1]
        $userId = $userId[3]

        $output = "$userName [$userId] is a new starter, their user account has been created however the email address is incorrect. Could you please update their email from $email to $displayName$domain`n`nPlease let me know if you need further details."

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\emailAddressChange.txt
        Add-Content -Path C:\TMP\ScriptOutput\emailAddressChange.txt -Value `n$end

    }
    
    end {
        
        Invoke-Item -Path C:\TMP\ScriptOutput\emailAddressChange.txt

    }
}