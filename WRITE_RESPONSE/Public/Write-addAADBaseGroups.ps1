function Write-addAADBaseGroups {
    [CmdletBinding()]
    param (
        [string[]] $deviceName
    )
    
    begin {
        
    }
    
    process {

        $devices = $deviceName -join "`n"
        
        $end = "Thanks, Craig"

        $output = "Please add the following devices to the following Intune AD Groups: `n`n_DATACOM BASE APPS - DEVICES `n_DATACOM - PAG DEVICES`n"

        # Write request data to file
        Set-Content -Value $output -Path C:\TMP\ScriptOutput\AddAADBaseGroups.txt
        Add-Content -Path C:\TMP\ScriptOutput\AddAADBaseGroups.txt -Value `n$devices`n
        Add-Content -Path C:\TMP\ScriptOutput\AddAADBaseGroups.txt -Value `n$end        
    }
    
    end {
        Get-Content -Path C:\TMP\ScriptOutput\AddAADBaseGroups.txt | clip
    }
}