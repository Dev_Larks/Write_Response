function Write-NAPCClosure {
    [CmdletBinding(DefaultParameterSetName = 'firstPerson')]
    param (

        [Parameter(ParameterSetName = 'firstPerson', Mandatory = $true)]
        [Parameter(ParameterSetName = 'thirdPerson', Mandatory = $true)] 
        [string] $firstName,

        [Parameter(ParameterSetName = 'firstPerson', Mandatory = $true)]
        [Parameter(ParameterSetName = 'thirdPerson', Mandatory = $true)]
        [string] $userToCompare,

        [Parameter(ParameterSetName = 'firstPerson', Mandatory = $true)]
        [Parameter(ParameterSetName = 'thirdPerson', Mandatory = $true)] 
        [string] $drive,

        [Parameter(ParameterSetName = 'thirdPerson', Mandatory = $true)]
        [string]$userName

    )
    
    begin {

    }
    
    process {

        # Call private helper function to get salutation
        $start = Get-Greeting
        
        $end = "Kind Regards `n`nCraig"

        if ($userName) {
            $output = "I will close off this request as the ticket I logged on your behalf for $userName to be assigned the same $drive access as $userToCompare has now been completed."
        }
        else {
            $output = "I will close off this request as the ticket I logged on your behalf for you to be assigned the same $drive access as $userToCompare has now been completed."
        }

        # Write request data to file
        Set-Content -Value $start -Path C:\TMP\ScriptOutput\NAPCClosure.txt
        Add-Content -Path C:\TMP\ScriptOutput\NAPCClosure.txt -Value `n$output 
        Add-Content -Path C:\TMP\ScriptOutput\NAPCClosure.txt -Value `n$end
    }
    
    end {
        Invoke-Item -Path C:\TMP\ScriptOutput\NAPCClosure.txt
    }
}