[CmdletBinding()]
param(
    [ValidateSet('Major', 'Minor', 'Patch')]
    [string]$Bump
)

$manifestPath = [IO.Path]::Combine($PSScriptRoot, 'WRITE_RESPONSE', 'WRITE_RESPONSE.psd1')
$moduleVersion = (Test-ModuleManifest -Path $manifestPath).Version
$major = $moduleVersion.Major
$minor = $moduleVersion.Minor
$patch = $moduleVersion.Build

if ($Bump) {
    switch ($Bump) {
        'Major' {
            $major++
            $minor = 0
            $patch = 0
            break
        }
        'Minor' {
            $minor++
            $patch = 0
            break
        }
        'Patch' {
            $patch++
            break
        }
        default {}
    }
    $newVersion = [version]"$major.$minor.$patch"
    Write-Verbose "Bumping module version to [$newVersion]"
    Update-ModuleManifest -Path $manifestPath -ModuleVersion $newVersion
    $moduleVersion = (Test-ModuleManifest -Path $manifestPath).Version
}

# Create output directory
$outputDir = [IO.Path]::Combine($PSScriptRoot, 'Output')
New-Item -Path $outputDir -ItemType Directory -Force | Out-Null

# Create version directory in output directory
# Make sure the directory is empty if it already exists
$versionOutputDir = [IO.Path]::Combine($outputDir , $moduleVersion)
if (Test-Path -Path $versionOutputDir) {
    Get-ChildItem -Path $versionOutputDir -Recurse | Remove-Item -Force
    Start-Sleep -Seconds 3
} else {
    New-Item -Path $versionOutputDir -ItemType Directory | Out-Null
}

# Concatenate all source files into root PSM1
$rootModule = [IO.Path]::Combine($versionOutputDir, 'WRITE_RESPONSE.psm1')
$sourceFiles = Get-ChildItem -Path ./WRITE_RESPONSE -Filter '*.ps1' -Exclude '*.tests.ps1' -Recurse
$sourceFiles | ForEach-Object {
    "# source: $($_.Name)"
    #Get-Content $_.FullName
    $_ | Get-Content
    ''
} | Add-Content -Path $rootModule -Encoding utf8

# Copy module manifest
$outputManifest = Copy-Item -Path $manifestPath -Destination $versionOutputDir -PassThru

# Update FunctionsToExport
$publicFunctions = Get-ChildItem -Path ./WRITE_RESPONSE/Public -Filter '*.ps1' -Recurse
Update-ModuleManifest -Path $outputManifest -FunctionsToExport $publicFunctions.BaseName